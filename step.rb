require 'xcodeproj'
require 'fileutils'

def resolve_user_defined_settings(path, build_settings)
  # Replace all occurrences of $(VARIABLE) with the corresponding value in build settings
  path.gsub(/\$\(([^)]+)\)/) do |match|
    variable_name = $1
    build_settings[variable_name] || match # Replace if found, or leave untouched if not found
  end
end

def find_project_level_setting(project, setting, configuration_name)
  project.root_object.build_configuration_list.build_configurations
    .select { |config| config.name == configuration_name } # Filter by configuration_name
    .map { |config| config.build_settings[setting] }
    .compact
    .first
end


begin
  project_path = ENV['project_path']
  target_name = ENV['target_name']
  configuration_name = ENV['build_configuration_name']

  unless File.directory?(project_path)
    puts project_path + " doesn't exists!"
    exit 1
  end

  ext = File.extname(project_path)

  unless ext == '.xcodeproj'
    puts project_path + ' is not a valid Xcode Project!'
    exit 1
  end

  project = Xcodeproj::Project.open(project_path)

  unless project
    puts project_path + ' is not a valid .xcodeproj!'
    exit 1
  end

  target_found = false
  configuration_found = false
  plist_path = ''
  bundle_identifier = ''
  enable_bitcode = false

  # Log inputs
  puts "INFO: project_path: #{project_path}"
  puts "INFO: target_name: #{target_name}"
  puts "INFO: configuration_name: #{configuration_name}"

  project.targets.each do |target_obj|
    next unless target_obj.name == target_name
    target_found = true
    target_obj.build_configuration_list.build_configurations.each do |build_configuration|
      next unless build_configuration.name == configuration_name
      configuration_found = true

      build_settings = build_configuration.build_settings
      if build_settings.key?('ENABLE_BITCODE')
        enable_bitcode = (build_settings['ENABLE_BITCODE'] == 'YES')
      end

      bundle_identifier = build_settings['PRODUCT_BUNDLE_IDENTIFIER']
      if !bundle_identifier
        puts 'WARN: Bundle identifier not found in target trying to find in project'
        bundle_identifier = find_project_level_setting(project, 'PRODUCT_BUNDLE_IDENTIFIER', configuration_name)
      end

      if bundle_identifier
        bundle_identifier = resolve_user_defined_settings(bundle_identifier, build_settings)
      end

      plist_path = build_settings['INFOPLIST_FILE'].dup

      puts "INFO: Found target (#{target_name}) and configuration (#{configuration_name}) in project: #{project_path}"

      if !plist_path
        puts 'WARN: Plist path not found in target trying to find in project'
        plist_path = find_project_level_setting(project, 'INFOPLIST_FILE', configuration_name)
      end
      # Check if plist_path is defined
      if plist_path
        plist_path = resolve_user_defined_settings(plist_path, build_settings)
        plist_path.gsub! '$(SRCROOT)/', ''
        plist_path = "#{File.dirname(project_path)}/#{plist_path}"
        puts "INFO: Plist path: #{plist_path}"
      else
        puts 'WARN: Plist path not found in project'
      end
    end
  end

  raise "target (#{target_name}) not found in project: #{project_path}" unless target_found
  raise "configuration (#{configuration_name}) does not exist in project: #{project_path}" unless configuration_found

  project.save

  # Check if plist_path is defined
  if !plist_path.to_s.empty?
    plist_file = Xcodeproj::Plist.read_from_path(plist_path)
    identifier = plist_file['CFBundleIdentifier']
    bundle_identifier = identifier unless identifier == '$(PRODUCT_BUNDLE_IDENTIFIER)'
  end


  puts 'WARN: Bundle identifier not found' if bundle_identifier.to_s.empty?

  outputs = {
    'XCODE_PLIST_PATH' => plist_path,
    'BITCODE_ENABLED' => enable_bitcode ? 'yes' : 'no',
    'BUNDLE_IDENTIFIER' => bundle_identifier
  }

  outputs.each do |key, value|
    `envman add --key "#{key}" --value "#{value}"`
    puts "#{key}=#{value}"
  end
rescue StandardError => ex
  puts
  puts 'Error:'
  puts ex.to_s
  puts
  puts 'Stacktrace (for debugging):'
  puts ex.backtrace.join("\n").to_s
  exit 1
end
